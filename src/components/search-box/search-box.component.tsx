import { ChangeEventHandler } from 'react';

import './search-box.styles.css'

type SearchBoxProps = {
  className: string;
  placeholder?: string;
  onchangeHandler: ChangeEventHandler<HTMLInputElement>;
}

const SearchBox = ({ 
  onchangeHandler, 
  placeholder, 
  className 
}: SearchBoxProps) => {
  return(
    <input
      className={`search-box ${className}`}
      type='search'
      placeholder={placeholder}
      onChange={onchangeHandler} 
    />
  );
}

export default SearchBox;