import Card from '../card/card.component';

import './card-list.styles.css';

type Monster = { 
  id: string;
  name: string;
  email: string;
}

type CardListProps = { 
  monsters : Monster[]
}

const CardList = ({monsters}: CardListProps) => ( // {monsters}  equi vaut à props en haut puis const {monsters} = props;
  // implicit return

  <div className='card-list'>
    {
      monsters.map((monster) => {
        return (
          <Card monster={monster} key={monster.id}/>
        )
      })
    }
  </div>
);

export default CardList;