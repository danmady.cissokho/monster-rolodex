import './card.styles.css';

type Monster = { 
  id: string;
  name: string;
  email: string;
}

type CardProps = { 
  monster : Monster
}
// const Card = (props) => {
//   const { name, email, id } = props.monster;
const Card = ({monster}: CardProps) => {
  const { name, email, id } = monster;
  return(
    <div  className='card-container' key={id}>
      <img 
        alt={`monster ${name}`} 
        src={`https://robohash.org/${id}?set=set2&size=180x180`}
      />
      <h2>{name}</h2>
      <p>{email}</p>
    </div>
  );
};

export default Card;