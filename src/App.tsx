import { useState, useEffect, ChangeEvent } from 'react';

import CardList from './components/card-list/card-list.component';
import SearchBox from './components/search-box/search-box.component';
import { getData } from './utils/data.utils';

import './App.css';

type Monster = { 
  id: string;
  name: string;
  email: string;
}

const App = () => {

  const [searchField, setSearchField] = useState('');
  const [title, setTitle] = useState('');
  const [monsters, setMonsters] = useState<Monster[]>([]);
  const [filteredMonsters, setFilteredMonsters] = useState(monsters);

  useEffect(() => {
    const fetchUsers = async () => {
      const users = await getData<Monster[]>(
        'https://jsonplaceholder.typicode.com/users'
      );
      setMonsters(users);
    };
    fetchUsers();
  }, []);
 
  useEffect(() => {
    let filteredMonsters = monsters.filter((monster: Monster) => {
      return monster.name.toLocaleLowerCase().includes(searchField) 
    });
    setFilteredMonsters(filteredMonsters)
  }, [monsters, searchField]); 

  const  onSearchChange = (event: ChangeEvent<HTMLInputElement>): void => {
    const searchFieldString = event.target.value.toLocaleLowerCase();
    setSearchField(searchFieldString);
  } 

  const  onTitleChange = (event: ChangeEvent<HTMLInputElement>): void  => {
    const TitleFieldString = event.target.value.toLocaleLowerCase();
    setTitle(TitleFieldString);
  } 

  return (
    <div className="App">
      <h1 className='app-title'>{title}</h1>
      <SearchBox 
        onchangeHandler={onSearchChange} 
        placeholder='search monstrers'
        className='monsters-search-box'
      />
      <br />
      <SearchBox 
        onchangeHandler={onTitleChange} 
        placeholder='Title monstrers'
        className='title-search-box'
      />
      <CardList monsters={filteredMonsters}/>
    </div>
  );

}

export default App;
